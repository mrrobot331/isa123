package rs.ac.singidunum.isa.app.dto;

import java.util.ArrayList;

public class AutoriDTO {
	private Long id;
	private ArrayList<NaucniRadDTO> naucniRadovi;
	
	public AutoriDTO() {
		super();
		// TODO Auto-generated constructor stub
	}


	public AutoriDTO(Long id, ArrayList<NaucniRadDTO> naucniRadovi) {
		super();
		this.id = id;
		this.naucniRadovi = naucniRadovi;
	}


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public ArrayList<NaucniRadDTO> getNaucniRadovi() {
		return naucniRadovi;
	}


	public void setNaucniRadovi(ArrayList<NaucniRadDTO> naucniRadovi) {
		this.naucniRadovi = naucniRadovi;
	}
	
}
