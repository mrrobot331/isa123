package rs.ac.singidunum.isa.app.dto;



import rs.ac.singidunum.isa.app.model.NaucniRad;

public class KategorijaDTO {
	private Long id;
	private String naziv;
	private NaucniRad naucniRad;

	public KategorijaDTO(Long id, String naziv, NaucniRad naucniRad) {
		super();
		this.id = id;
		this.naziv = naziv;
		this.naucniRad = naucniRad;
	}

	public KategorijaDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public NaucniRad getNaucniRad() {
		return naucniRad;
	}

	public void setNaucniRad(NaucniRad naucniRad) {
		this.naucniRad = naucniRad;
	}
	
}
