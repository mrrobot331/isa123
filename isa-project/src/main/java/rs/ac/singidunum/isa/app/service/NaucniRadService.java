package rs.ac.singidunum.isa.app.service;

import org.springframework.stereotype.Service;

import rs.ac.singidunum.isa.app.model.NaucniRad;
import rs.ac.singidunum.isa.app.repository.NaucniRadRepository;
@Service
public class NaucniRadService extends GenerateService<NaucniRadRepository, NaucniRad, Long> {

}
