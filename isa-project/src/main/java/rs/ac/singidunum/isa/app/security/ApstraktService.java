package rs.ac.singidunum.isa.app.security;

import org.springframework.stereotype.Service;

import rs.ac.singidunum.isa.app.model.Apstrakt;
import rs.ac.singidunum.isa.app.repository.ApstraktRepository;
import rs.ac.singidunum.isa.app.service.GenerateService;

@Service
public class ApstraktService extends GenerateService<ApstraktRepository, Apstrakt, Long>{

}
