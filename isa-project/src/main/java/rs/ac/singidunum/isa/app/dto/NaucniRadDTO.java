package rs.ac.singidunum.isa.app.dto;

import java.util.Set;


public class NaucniRadDTO {
private Long id;
	private String naziv;
	private Set<AutoriDTO> autori;
	private Set<KljucneReciDTO> kljucneReci;
	private ApstraktDTO apstrakt;
	
	public NaucniRadDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	public NaucniRadDTO(Long id, String naziv, Set<AutoriDTO> autori, Set<KljucneReciDTO> kljucneReci,
			ApstraktDTO apstrakt) {
		super();
		this.id = id;
		this.naziv = naziv;
		this.autori = autori;
		this.kljucneReci = kljucneReci;
		this.apstrakt = apstrakt;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNaziv() {
		return naziv;
	}
	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}
	public Set<AutoriDTO> getAutori() {
		return autori;
	}
	public void setAutori(Set<AutoriDTO> autori) {
		this.autori = autori;
	}
	public Set<KljucneReciDTO> getKljucneReci() {
		return kljucneReci;
	}
	public void setKljucneReci(Set<KljucneReciDTO> kljucneReci) {
		this.kljucneReci = kljucneReci;
	}
	public ApstraktDTO getApstrakt() {
		return apstrakt;
	}
	public void setApstrakt(ApstraktDTO apstrakt) {
		this.apstrakt = apstrakt;
	}
	
	
	
}
